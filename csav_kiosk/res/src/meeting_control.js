function launch_meeting(mode, number)
{
	// let cmd = "ka 01 01\r";
	// let enc = new TextEncoder();
 //    let buf = enc.encode(cmd);
                
	// chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
	clearInterval(mtint);
	clearInterval(bjint);

	document.getElementsByClassName('vtc_status')[0].style.backgroundImage = "url('../icons/vtc_use.png')";

	if (mode == "bj")
	{
		console.log("BlueJeans call: " + number);
		let webview = chrome.app.window.get("ext").contentWindow.document.getElementsByTagName("webview")[0];
		webview.src = "https://www.bluejeans.com/" + number + "?embed=true&fullscreen=true&name=Conference%20Room";
		webview.addEventListener('permissionrequest', function(e)
		{
			if (e.permission == "media")
				e.request.allow();
		});
		bjint = setInterval(function()
		{
			// UNMUTES ON ENTRY
			webview.executeScript(
			{
				code: "if (window.getComputedStyle(document.getElementsByClassName('primaryOption joinComputer')[0].parentElement).width != 'auto')" +
						"document.getElementsByClassName('primaryOption joinComputer')[0].click();" +
						"setTimeout(function() {if (document.getElementsByClassName('enter')[0].style.display != 'none') {" +
						"document.getElementsByClassName('enter')[0].click();}}, 5000);"
			});
			webview.executeScript(
			{
				code: "if (document.getElementsByClassName('slideOutPanel fullheight icon_labels')[0] != undefined)" +
						"{ document.getElementsByClassName('slideOutPanel fullheight icon_labels')[0].style.display = 'none';}"
			});
			webview.executeScript(
			{
				code: "if (document.getElementsByClassName('toolbar fullheight icon_labels')[0] != undefined)" +
						"{ document.getElementsByClassName('toolbar fullheight icon_labels')[0].style.display = 'none';}"
			});
		}, 3000);
	}
	else
	{
		let webview = chrome.app.window.get("ext").contentWindow.document.getElementsByTagName("webview")[0];
		let new_meet = false;
		if (number == "")
		{
			webview.src = "https://meet.google.com/"
			new_meet = true;
		}
		else
			webview.src = "https://meet.google.com/" + number;
		webview.addEventListener('permissionrequest', function(e)
		{
			if (e.permission == "media")
				e.request.allow();
		});
		mtint = setInterval(function()
		{
			webview.executeScript(
			{
				code: "for (let i = 0; i < document.getElementsByClassName('RveJvd snByac').length; i++)" +
				"{ if (document.getElementsByClassName('RveJvd snByac')[i].textContent == 'Admit') {document.getElementsByClassName('RveJvd snByac')[i].click();}}"
			});
			if (new_meet)
			{
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('div').length; i++)" +
					"{ if (document.getElementsByTagName('div')[i].textContent == 'Start a new meeting') {document.getElementsByTagName('div')[i].click();} }"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('span').length; i++)" +
					"{ if (document.getElementsByTagName('span')[i].textContent == 'Start meeting') {document.getElementsByTagName('span')[i].click();} }"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('span').length; i++)" +
					"{ if (document.getElementsByTagName('span')[i].textContent == 'Copy joining info') {document.getElementsByTagName('span')[i].parentElement.parentElement.parentElement.removeChild(document.getElementsByTagName('span')[i].parentElement.parentElement);} }"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('span').length; i++)" +
					"{ if (document.getElementsByTagName('span')[i].textContent == 'Add someone by phone') {document.getElementsByTagName('span')[i].parentElement.parentElement.parentElement.removeChild(document.getElementsByTagName('span')[i].parentElement.parentElement);} }"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('div').length; i++)" +
					"{ if (document.getElementsByTagName('div')[i].textContent == 'Add others') {document.getElementsByTagName('div')[i].parentElement.style.position = 'absolute'; document.getElementsByTagName('div')[i].parentElement.style.top = '0'; document.getElementsByTagName('div')[i].parentElement.style.left = '0'; document.getElementsByTagName('div')[i].parentElement.style.opacity = '0';} }"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('div').length; i++)" +
					"{ if (document.getElementsByTagName('div')[i].textContent == 'Add others') {document.getElementsByTagName('div')[i].style.display = 'none';} }"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('div').length; i++)" +
					"{ if (document.getElementsByTagName('div')[i].textContent == 'Share this info with people you want in the meeting') {document.getElementsByTagName('div')[i].style.display = 'none';} }"
				});
				webview.executeScript(
				{
					code: "document.getElementsByClassName('kCtYwe vVSjvc')[1].style.display = 'none';"
				});
				webview.executeScript(
				{
					code: "document.getElementsByClassName('mjANdc eEPege')[0].style.backgroundColor = 'rgba(0,0,0,0)'; document.getElementsByClassName('llhEMd iWO5td')[0].style.backgroundColor = 'rgba(0,0,0,0)';"
				});
				webview.executeScript(
				{
					code: "for (let i = 0; i < document.getElementsByTagName('div').length; i++) { if (document.getElementsByTagName('div')[i].textContent == 'Meeting details') {document.getElementsByTagName('div')[i].parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.style.display = 'none';} }"
				});
				webview.executeScript(
			  {
			    code: "for (let i = 0; i < document.getElementsByTagName('span').length; i++) { if (document.getElementsByTagName('span')[i].textContent == 'Dial-in: ') {window.location.href = window.location.href + '#' + window.location.pathname.substring(1, window.location.pathname.length) + '.' + document.getElementsByTagName('span')[i].parentElement.children[1].textContent + '.' + document.getElementsByTagName('span')[i].parentElement.children[3].textContent;}}"
			  }, function()
			  {
			    if (webview.src.indexOf('#') > -1)
			    {
			      let meet_meta = webview.src.substring(webview.src.indexOf('#') + 1, webview.src.length);
			      let meet_id = meet_meta.substring(0, meet_meta.indexOf('.'));
			      let meet_phone = decodeURIComponent(meet_meta.substring(meet_meta.indexOf('.') + 1, meet_meta.indexOf('#')));
			      let meet_number = meet_phone.substring(0, meet_phone.indexOf('.'));
			      let meet_pin = meet_phone.substring(meet_phone.indexOf('.') + 1, meet_phone.length);
			      
			      let info_text = "Meet ID: " + meet_id + "<br>" + "Dial-in: " + meet_number + "<br>" + "PIN: " + meet_pin;
			      document.getElementsByClassName('text')[9].innerHTML = info_text;
			      
			      console.log(meet_id);
			      console.log(meet_number);
			      console.log(meet_pin);
			    }
			  });
			}
			webview.executeScript(
			{
				code: "for (let i = 0; i < document.getElementsByTagName('span').length; i++)" +
				"{ if (document.getElementsByTagName('span')[i].textContent == 'Join meeting') {document.getElementsByTagName('span')[i].click();} }"
			});
		}, 1000);
	}
}