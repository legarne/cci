setInterval(function()
{
	let d = new Date();
	d = d.toLocaleTimeString().substring(0, d.toLocaleTimeString().lastIndexOf(":")) + d.toLocaleTimeString().substring(d.toLocaleTimeString().lastIndexOf(" "), d.toLocaleTimeString().length);
	document.getElementsByClassName("header_text text")[document.getElementsByClassName("header_text text").length - 1].textContent = d;
}, 500);

function capitalize(string) {
    return string[0].toUpperCase() + string.slice(1);
}
function set_mirror_off()
{
	settings.mirror_mode_act = false;
	chrome.system.display.setMirrorMode({mode:"off"}, function()
	{
	 // setInterval(function()
	 // {
	 	chrome.system.display.getInfo(function(displayInfo)
		{
			{
				let mon_0 = {};
				mon_0.width = displayInfo[0].bounds.width;
				mon_0.height = displayInfo[0].bounds.height;
				mon_0.top = displayInfo[0].bounds.top;
				mon_0.left = displayInfo[0].bounds.left;

				let mon_1 = {};
				mon_1.width = displayInfo[1].bounds.width;
				mon_1.height = displayInfo[1].bounds.height;
				mon_1.top = displayInfo[1].bounds.top;
				mon_1.left = displayInfo[1].bounds.left;

				// establish primary monitor
				if (mon_0.width < mon_1.width)
				{
					let main_window = chrome.app.window.get("main");
					let ext_window = chrome.app.window.get("ext");
					main_window.innerBounds = {minWidth: mon_0.width, minHeight: mon_0.height, maxWidth: mon_0.width, maxHeight: mon_0.height};
					main_window.innerBounds.setPosition(mon_0.left, mon_0.top);
					ext_window.innerBounds = {minWidth: mon_1.width, minHeight: mon_1.height, maxWidth: mon_1.width, maxHeight: mon_1.height, left: mon_1.left, top: mon_1.top};
				}
				else
				{
				  let main_window = chrome.app.window.get("main");
					let ext_window = chrome.app.window.get("ext");
					main_window.innerBounds = {minWidth: mon_1.width, minHeight: mon_1.height, maxWidth: mon_1.width, maxHeight: mon_1.height};
					main_window.outerBounds.setPosition(mon_1.left, mon_1.top);
					ext_window.innerBounds = {minWidth: mon_0.width, minHeight: mon_0.height, maxWidth: mon_0.width, maxHeight: mon_0.height};
					ext_window.outerBounds.setPosition(mon_0.left, mon_0.top);
				}
			}
		});
	 // }, 1000);
	});
}
// --------------------------------------------------------------------------------------

var dialer_settings = {mode : "bj"};
var settings = {};
settings.p_vol = 0;
settings.sock = null;
var schedule_int = null;
var refresh_int = null;
var fullscreen_counter = 0;
var is_fullscreen = false;
var mtint;
var bjint;
var remote_mode = "";
var tcp_time_check;

function control_connect()
{
	chrome.sockets.tcp.create({}, function(createInfo)
	{
		chrome.sockets.tcp.connect(createInfo.socketId, "192.168.1.111", 5566, function()
		{
			settings.sock = createInfo.socketId;
			tcp_time_check = setInterval(function()
			{
				let cmd = "c0";
				let enc = new TextEncoder();
          		let buf = enc.encode(cmd);
			                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('TCP TIME REFRESH')});
			}, 1000);
		});
	});
}

function load_app(type)
{
	document.getElementsByClassName("app_name")[0].textContent = capitalize(type);
	let inner_content = document.getElementsByClassName("inner_content")[0];
	while (inner_content.firstChild)
		inner_content.removeChild(inner_content.firstChild);

	inner_content.innerHTML = apps[type];
	// Type Dial
	if (type != "dialer")
	{
		clearInterval(schedule_int);
		schedule_int = null;
		clearInterval(refresh_int);
		refresh_int = null;
	}
	if (type == "dialer")
	{
		set_mirror_off();
		if (schedule_int == null)
		{
			schedule_int = setInterval(function()
			{
				let webview_d = document.getElementsByTagName('webview')[0];
				if (webview_d.src.indexOf("#") > -1)
				{
					// Dial Here...
					console.log(webview_d.src);
					let raw = webview_d.src.substring(webview_d.src.indexOf('#') + 1, webview_d.src.length);
					console.log(raw);
					let meet_id = raw.substring(0, raw.indexOf('.'));
					console.log(meet_id);
					let phone = raw.substring(raw.indexOf('.') + 1, raw.length);
					phone = decodeURIComponent(phone);
					phone = phone.substring(0, phone.indexOf('.'));
					console.log(phone);
					let pin = raw.substring(raw.lastIndexOf('.') + 1, raw.length);
					console.log(pin);
					launch_meeting("meet", meet_id);
					
					let info_text = "Meet ID: " + meet_id + "<br>" + "Dial-in: " + phone + "<br>" + "PIN: " + pin;
			      	document.getElementsByClassName('text')[9].innerHTML = info_text;

					webview_d.src = "https://dialer.withsolutionsrd.com/schgen/?tok=K221QzDVALCSX0SLYhmEABH9Pm";
				}
			}, 100);
		}
		if (refresh_int == null)
		{
			refresh_int = setInterval(function()
			{
				let webview_d = document.getElementsByTagName('webview')[0];
				webview_d.src = "";
				webview_d.src = "https://dialer.withsolutionsrd.com/schgen/?tok=K221QzDVALCSX0SLYhmEABH9Pm";
			}, 1000 * 60);
		}
	}
	// Mirror Mode here;
    if (type == "dialer" || type == "control" || type == "account")
    {
        document.getElementsByClassName("mirror_mode")[0].style.display = 'none';
    }
	if (type == "chrome")
	{
		document.getElementsByClassName("mirror_mode")[0].style.display = 'block';
	}
	else if (type == "gmail")
	{
		document.getElementsByClassName("mirror_mode")[0].style.display = 'block';
		let webview = document.getElementsByClassName("full_webview")[0];
		webview.addEventListener('newwindow', function(e)
		{
			webview.src = e.targetUrl;
		});
	}
	else if (type == "drive")
	{
		document.getElementsByClassName("mirror_mode")[0].style.display = 'block';
		let webview = document.getElementsByClassName("full_webview")[0];
		webview.addEventListener('newwindow', function(e)
		{
			webview.src = e.targetUrl;
		});
		webview.addEventListener('permissionrequest', function(e)
		{
			if (e.permission == 'fullscreen')
			{
				e.request.allow();
			}
		});
	}
	else if (type == "jamboard")
	{
		document.getElementsByClassName("mirror_mode")[0].style.display = 'block';
		let webview = document.getElementsByClassName("full_webview")[0];
		webview.addEventListener('contentload', function()
		{
			webview.executeScript(
			{
				code: "document.getElementsByClassName('jam-list-create-jam-button')[0].style.position = 'absolute';" +
					  "document.getElementsByClassName('jam-list-create-jam-button')[0].style.top = (parseInt(window.innerHeight) - 150) + 'px';" +
					  "document.getElementsByClassName('jam-list-msg-noitems-heading')[0].style.position = 'absolute';" +
					  "document.getElementsByClassName('jam-list-msg-noitems-heading')[0].style.top = (parseInt(window.innerHeight) - 150) + 'px';" +
					  "document.getElementsByClassName('jam-list-msg-noitems-heading')[0].style.left = '25px';" +
					  "document.getElementsByClassName('jam-list-msg-noitems-body')[0].style.position = 'absolute';" +
					  "document.getElementsByClassName('jam-list-msg-noitems-body')[0].style.top = (parseInt(window.innerHeight) - 130) + 'px';" +
					  "document.getElementsByClassName('jam-list-msg-noitems-body')[0].style.left = '25px';"
			});
		});
	}
    if (type == "control")
    {
		if (settings.sock != null)
		{
			chrome.sockets.tcp.disconnect(settings.sock, function()
			{
				settings.sock = null;
				control_connect();
				clearInterval(tcp_time_check);
			});
		}
		else
		{
			control_connect();
			clearInterval(tcp_time_check);
		}
    	function load_ctrl_page()
    	{
    		let cci_start = document.getElementsByClassName('cci_start')[0];
			while(cci_start.firstChild)
	    		cci_start.removeChild(cci_start.firstChild);
    		
    		settings.ctrl_load = true;
            let dev_img = document.createElement("div");
            dev_img.className = 'dev_img overbox';
            cci_start.appendChild(dev_img);
            
            anim(dev_img, {opacity: 1}, .75);

            dev_img = document.getElementsByClassName('dev_img')[0];
            dev_img.style.top = (parseInt(cci_start.clientHeight) / 2.18) - (parseInt(dev_img.clientHeight) / 2) + 'px';
            
            let underbox = document.createElement('div');
            underbox.className = 'underbox';

            dev_img.appendChild(underbox);

            let sec_0 = document.createElement('div');
            let sec_1 = document.createElement('div');
            let sec_2 = document.createElement('div');

            sec_0.style.width = '20%';
            sec_0.style.height = '100%';
            // sec_0.style.backgroundColor = 'red';
            sec_0.style.float = 'left';
            sec_1.style.width = '60%';
            sec_1.style.height = '100%';
			sec_1.style.backgroundColor = '#252E33';
			// sec_1.style.borderStyle = 'solid';
			// sec_1.style.borderWidth = '1px';
			// sec_1.style.borderColor = 'white';
			sec_1.style.float = 'left';
            sec_2.style.width = '20%';
            sec_2.style.height = '100%';
            // sec_2.style.backgroundColor = 'blue';
            sec_2.style.float = 'right';

            underbox.appendChild(sec_0);
            underbox.appendChild(sec_1);
            underbox.appendChild(sec_2);

            let input_0 = document.createElement('div');
            input_0.className = 'cci_button image overbox';
            input_0.style.backgroundImage = 'url("../icons/chrome_icon.png")';
            input_0.style.backgroundSize = '70% 100%';
            input_0.style.width = '80%';
            input_0.style.height = '20%';
            input_0.style.margin = '55% auto';
            sec_0.appendChild(input_0);

			let input_1 = document.createElement('div');
			input_1.className = 'cci_button image overbox';
			input_1.style.backgroundImage = 'url("../icons/hdmi.png")';
            input_1.style.backgroundSize = '70% 100%';
            input_1.style.width = '80%';
            input_1.style.height = '20%';
            input_1.style.margin = '60% auto';
            sec_0.appendChild(input_1);

            let vol_up = document.createElement('div');
            vol_up.className = 'cci_button image overbox';
            vol_up.style.backgroundImage = 'url("../icons/volup.png")';
            vol_up.style.backgroundSize = '70% 100%';
            vol_up.style.width = '80%';
            vol_up.style.height = '20%';
            vol_up.style.margin = '55% auto';
            sec_2.appendChild(vol_up);

			let vol_down = document.createElement('div');
			vol_down.className = 'cci_button image overbox';
			vol_down.style.backgroundImage = 'url("../icons/voldown.png")';
            vol_down.style.backgroundSize = '70% 100%';
            vol_down.style.width = '80%';
            vol_down.style.height = '20%';
            vol_down.style.margin = '60% auto';
            sec_2.appendChild(vol_down);

			let input_view = document.createElement('div');
			input_view.className = 'cci_button image overbox';
			input_view.style.backgroundImage = 'url("../icons/chrome_icon.png")';
            input_view.style.backgroundSize = '100% 100%';
            input_view.style.width = '80%';
            input_view.style.height = '80%';
            input_view.style.margin = '10% auto';
            sec_1.appendChild(input_view);

		    vol_up.onclick = function()
		    {
        		let cmd = "RS232CMD.mc 01 02";
				let enc = new TextEncoder();
          		let buf = enc.encode(cmd);
			                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
  			};
		    vol_down.onclick = function()
		    {
	        	let cmd = "RS232CMD.mc 01 03";
				let enc = new TextEncoder();
	        	let buf = enc.encode(cmd);
					                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
		    };
            input_0.onclick = function()
			{
				input_view.style.backgroundImage = 'url("../icons/chrome_icon.png")';
				let cmd = "RS232CMD.xb 01 90";
				let enc = new TextEncoder();
                let buf = enc.encode(cmd);
		                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
			};
			input_1.onclick = function()
			{
				input_view.style.backgroundImage = 'url("../icons/hdmi.png")';

				let cmd = "RS232CMD.xb 01 91";
				let enc = new TextEncoder();
                let buf = enc.encode(cmd);
                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
			};

			let power_off = document.createElement('div');
			power_off.className = 'cci_button image overbox';
			power_off.style.backgroundImage = 'url("../icons/power.png")';
            power_off.style.backgroundSize = '100% 100%';
            power_off.style.width = '100px';
            power_off.style.height = '100px';
            power_off.style.margin = '0 auto';
            power_off.style.position = 'relative';
            power_off.style.top = '93%';
            power_off.onclick = function()
            {
				let cmd = "RS232CMD.ka 01 00";
				let enc = new TextEncoder();
                let buf = enc.encode(cmd);
                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
				settings.ctrl_load = false;
				load_app('chrome');
            };
            underbox.appendChild(power_off);

    	}
    	if (!settings.ctrl_load)
    	{
			let logo = document.getElementsByClassName('cci_logo')[0];
	        logo.style.position = 'relative';
	        logo.style.top = (parseInt(logo.parentElement.clientHeight / 2) - parseInt(logo.clientHeight)) + 'px';
	        
	        let cci_text = document.getElementsByClassName('cci_logo_text')[0];
	        cci_text.style.position = 'relative';
	        cci_text.style.top = (parseInt(cci_text.parentElement.clientHeight / 2) - parseInt(cci_text.clientHeight)) + 'px';
	        
	        let cci_start = document.getElementsByClassName('cci_start')[0];
	        cci_start.onclick = function()
	        {
	            let loader = document.getElementsByClassName('cci_loader')[0];
	            anim(loader, {opacity: 1}, 1);
	            
	            anim(logo, {top: parseInt(logo.style.top) + 35}, .25).anim(function()
	            {
	                
	                anim(logo, {top: parseInt(logo.style.top) - 400}, 1);
	                anim(logo, {opacity: 0}, .5);
	                anim(cci_text, {top: parseInt(cci_text.style.top) - 900}, 1);
	                anim(cci_text, {opacity: 0}, .5);
	            });
				let cmd = "RS232CMD.ka 01 01";
				let enc = new TextEncoder();
                let buf = enc.encode(cmd);
                
				chrome.sockets.tcp.send(settings.sock, buf, function() {console.log('sent')});
	            setTimeout(function()
	            {
	                load_ctrl_page();
	            }, 2000);
	                
	        };
    	}
    	else
    	{
    		load_ctrl_page();
    	}
    }
	if (type == "dialer")
	{
	    document.getElementsByClassName('text')[3].style.display = 'none';
	    if (dialer_settings.mode == "meet")
	    	document.getElementsByClassName('text')[9].innerHTML = 'Meeting details will be<br> shown below.';
	    else
	    	document.getElementsByClassName('text')[9].innerHTML = "";

	    let mutes = document.getElementsByClassName('mute');
		mutes[0].onclick = function()
		{
			let webview = chrome.app.window.get("ext").contentWindow.document.getElementsByTagName("webview")[0];
			webview.executeScript
			({code: 
				"var audio_mute = document.getElementsByClassName('mute');" +
				"audio_mute[1].click();"
			});
			webview.executeScript(
			{
				code: "document.getElementsByClassName('DPvwYc JnDFsc Rz1Waf')[0].parentElement.click();"
			});
			this.className = this.className + " pressed";
			setTimeout(function()
			{
				document.getElementsByClassName('mute')[0].className = "mute audio_m";
			}, 200);

		}
		mutes[1].onclick = function()
		{
			let webview = chrome.app.window.get("ext").contentWindow.document.getElementsByTagName("webview")[0];
			webview.executeScript
			({code: 
				"var audio_mute = document.getElementsByClassName('mute');" +
				"audio_mute[0].click();"
			});
			webview.executeScript(
			{
				code: "document.getElementsByClassName('DPvwYc JnDFsc Rz1Waf')[1].parentElement.click();"
			});
			this.className = this.className + " pressed";
			setTimeout(function()
			{
				document.getElementsByClassName('mute')[1].className = "mute video_m";
			}, 200);
		}
		document.getElementsByClassName('hoz_app_button overbox larger_text text')[1].onclick = function()
		{
			let webview = chrome.app.window.get("ext").contentWindow.document.getElementsByTagName("webview")[0];
			let par = webview.parentElement;
			par.removeChild(webview);
			let new_webview = document.createElement("webview");
			par.appendChild(new_webview);
			new_webview.style.position = 'absolute';
			new_webview.style.top = '0';
			new_webview.style.left = '0';
			new_webview.style.width = '100%';
			new_webview.style.height = '100%';
			document.getElementsByClassName('text')[9].innerHTML = "Meeting details will be<br>shown below.";
			document.getElementsByClassName('vtc_status')[0].style.backgroundImage = "url('../icons/vtc_open.png')";
			this.className = this.className + " pressed";
			setTimeout(function()
			{
				document.getElementsByClassName('hoz_app_button overbox larger_text text')[1].className = 'hoz_app_button overbox larger_text text';
			}, 200);
		};
		let dialer_buttons = document.getElementsByClassName("hoz_app_button");
		for (let i = 0; i < dialer_buttons.length - 1; i++)
		{
			// dialer_buttons[i].onmousedown = function()
			// {
			// 	this.className = "hoz_app_button pressed overbox larger_text text";
			// }
			dialer_buttons[i].onclick = function()
			{
				this.className = "hoz_app_button pressed overbox larger_text text";
				launch_meeting(dialer_settings.mode, document.getElementsByClassName("app_input")[0].value);
				setTimeout(function()
				{
					document.getElementsByClassName('hoz_app_button pressed overbox larger_text text')[0].className = "hoz_app_button overbox larger_text text";
				}, 200);
			}
			// dialer_buttons[i].onmouseout = function()
			// {
			// 	this.className = "hoz_app_button overbox larger_text text";
			// }
		}
		let toggle_button = document.getElementsByClassName("toggle")[0];
		if (dialer_settings.mode != undefined)
			toggle_button.className = (dialer_settings.mode == "bj") ? "toggle overbox" : "toggle_meet toggle overbox";
		if (dialer_settings.mode != undefined)
			toggle_button.style.top = (dialer_settings.mode == "bj") ? "-10px" : "50px";
		
		let webview_d = document.getElementsByTagName('webview')[0];
		if (dialer_settings.mode == 'bj')
			webview_d.style.opacity = '0';
		else
			webview_d.style.opacity = '1';

		toggle_button.onclick = function()
		{
			if (dialer_settings.mode == "bj" || dialer_settings.mode == undefined)
			{
				dialer_settings.mode = "meet";
				document.getElementsByClassName('text')[3].innerHTML = 'Leave blank to start a new meet, or enter your meeting ID.<br>You must be signed in to start a new meet.';
				this.className = "toggle_meet toggle overbox";
				anim(this, {top: 50}, .25);
				webview_d.style.opacity = '1';
				document.getElementsByClassName('text')[9].innerHTML = 'Meeting details will be<br> shown below.';
			}
			else
			{
				dialer_settings.mode = "bj";
				document.getElementsByClassName('text')[3].innerHTML = 'Enter your meeting # or id, select your type of meeting, <br>  and press call below to dial in.';
				this.className = "toggle overbox";
				anim(this, {top: -10}, .25);
				webview_d.style.opacity = '0';
				document.getElementsByClassName('text')[9].innerHTML = "";
			}
			document.getElementsByClassName("app_input")[0].value = "";
		}
	}
	else if (type == "account")
	{
		let dialer_buttons = document.getElementsByClassName("hoz_app_button");
		for (let i = 0; i < dialer_buttons.length; i++)
		{
			dialer_buttons[i].onmousedown = function()
			{
				this.className = "hoz_app_button pressed overbox larger_text text";
			}
			dialer_buttons[i].onmouseup = function()
			{
				this.className = "hoz_app_button overbox larger_text text";
				// replace with google oauth api
				if (this.textContent == "Sign In")
				{
					let webview = document.getElementsByClassName("app_webview");
					webview[0].src = "https://accounts.google.com/ServiceLogin";
					webview[0].style.width = (window.innerWidth) - (window.innerWidth * 7/100) + "px";
					webview[0].style.height = (window.innerHeight) - (window.innerHeight * 30/100) + "px";
				}
				else
				{
					let webview = document.getElementsByClassName("app_webview");
					webview[0].src = "https://www.google.com/accounts/Logout";
					webview[0].style.width = (window.innerWidth) - (window.innerWidth * 7/100) + "px";
					webview[0].style.height = (window.innerHeight) - (window.innerHeight * 30/100) + "px";
				}
			}
			dialer_buttons[i].onmouseout = function()
			{
				this.className = "hoz_app_button overbox larger_text text";
			}
		}
	}
	else if (type == "settings")
	{
		let settings_input = document.getElementsByClassName("settings_input");
		for (let i = 0; i < settings_input.length; i++)
		{
			if (settings_input[i].placeholder == "URL" && settings.url != undefined)
				settings_input[i].value = settings.url;
			else if (settings_input[i].placeholder == "IP Addr" && settings.cci_ip != undefined)
				settings_input[i].value = settings.cci_ip;
			else if (settings_input[i].placeholder == "Device Name" && settings.device_name != undefined)
				settings_input[i].value = settings.device_name;

			settings_input[i].oninput = function()
			{
				if (this.placeholder == "URL")
					settings.url = this.value;
				else if (this.placeholder == "IP Addr")
					settings.cci_ip = this.value;
				else if (this.placeholder == "Device Name")
					settings.device_name = this.value;
				console.log(settings);
			}
		}
	}
}

window.onload = function()
{

	setInterval(function()
	{
		let remote_ajax = new XMLHttpRequest();
		remote_ajax.onreadystatechange = function()
		{
			if (remote_ajax.readyState == XMLHttpRequest.DONE && remote_ajax.status == 200)
			{
				if (remote_mode == this.responseText)
					return;
				else
				{
					if (this.responseText == "control.powon")
						document.getElementsByClassName('cci_start')[0].click();
					else if (this.responseText == "control.powoff")
						document.getElementsByClassName('cci_button')[5].click();
					else if (this.responseText == "control.input_0")
						document.getElementsByClassName('cci_button')[0].click();
					else if (this.responseText == "control.input_1")
						document.getElementsByClassName('cci_button')[1].click();
					else if (this.responseText == "control.volup" || this.responseText == "control.volup+")
						document.getElementsByClassName('cci_button')[3].click();
					else if (this.responseText == "control.voldown" || this.responseText == "control.voldown+")
						document.getElementsByClassName('cci_button')[4].click();
					else
						load_app(this.responseText);
						
					remote_mode = this.responseText;
				}
			}
		};
		remote_ajax.open("POST", "https://www.dialer.withsolutionsrd.com/helpdesk/index.php");
		remote_ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		remote_ajax.send("type=r_control_get");

		// if (fullscreen_counter >= 5 && !is_fullscreen)
		// {
		// 	let inner_content = document.getElementsByClassName('inner_content')[0];
		// 	inner_content.className = "inner_content inner_content_full";
		// 	settings.mirror_mode_act = true;
		// 	chrome.system.display.setMirrorMode({mode:"normal"});
		// 	fullscreen_counter = 0;
		// 	is_fullscreen = true;
		// }
		// else if (fullscreen_counter >= 5 && is_fullscreen)
		// {
		// 	let inner_content = document.getElementsByClassName('inner_content')[0];
		// 	inner_content.className = "inner_content";
		// 	settings.mirror_mode_act = false;
		// 	set_mirror_off();
		// 	fullscreen_counter = 0;
		// 	is_fullscreen = false;	
		// }
		// else
		// 	fullscreen_counter = 0;
	}, 100);
	
	setInterval(function()
	{
		chrome.system.display.getInfo(function(displayInfo)
		{
			{
				let mon_0 = {};
				mon_0.width = displayInfo[0].bounds.width;
				mon_0.height = displayInfo[0].bounds.height;
				mon_0.top = displayInfo[0].bounds.top;
				mon_0.left = displayInfo[0].bounds.left;

				let mon_1 = {};
				mon_1.width = displayInfo[1].bounds.width;
				mon_1.height = displayInfo[1].bounds.height;
				mon_1.top = displayInfo[1].bounds.top;
				mon_1.left = displayInfo[1].bounds.left;

				// establish primary monitor
				if (mon_0.width < mon_1.width)
				{
					let main_window = chrome.app.window.get("main");
					let ext_window = chrome.app.window.get("ext");
					main_window.innerBounds = {minWidth: mon_0.width, minHeight: mon_0.height, maxWidth: mon_0.width, maxHeight: mon_0.height};
					main_window.innerBounds.setPosition(mon_0.left, mon_0.top);
					ext_window.innerBounds = {minWidth: mon_1.width, minHeight: mon_1.height, maxWidth: mon_1.width, maxHeight: mon_1.height, left: mon_1.left, top: mon_1.top};
				}
				else
				{
				  let main_window = chrome.app.window.get("main");
					let ext_window = chrome.app.window.get("ext");
					main_window.innerBounds = {minWidth: mon_1.width, minHeight: mon_1.height, maxWidth: mon_1.width, maxHeight: mon_1.height};
					main_window.outerBounds.setPosition(mon_1.left, mon_1.top);
					ext_window.innerBounds = {minWidth: mon_0.width, minHeight: mon_0.height, maxWidth: mon_0.width, maxHeight: mon_0.height};
					ext_window.outerBounds.setPosition(mon_0.left, mon_0.top);
				}
			}
		});
	}, 1000);
	// ****************************************
	// let CLIENT_ID = '1094807849484-fc651u05gpo6ufpap8h9l9ogqa7kc5qu.apps.googleusercontent.com';
 //  	let API_KEY = 'AIzaSyCtol6I9PAkDwWxUxwmvLXfVuZU5LMEJWQ';
 //  	var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"];
 //  	var SCOPES = "https://www.googleapis.com/auth/calendar.readonly";

	// function handleClientLoad() {
 //    	gapi.load('client:auth2', initClient);
 //  	}
 //  	function initClient()
 //  	{
 //    	gapi.client.init(
 //    	{
 //        	apiKey: API_KEY,
 //        	clientId: CLIENT_ID,
 //        	discoveryDocs: DISCOVERY_DOCS,
 //        	scope: SCOPES
 //        }).then(function ()
 //        {
 //        	let si_status = gapi.auth2.getAuthInstance().isSignedIn.get();
 //        	if (!si_status)
 //        		gapi.auth2.getAuthInstance().signIn();
 //        	else
 //        	{

 //        	}
 //        }, function(error)
 //        {
 //        	console.log(error);
 //        });
 //    }

	// event listeners
	let app_menu_buttons = document.getElementsByClassName("app_menu_button");
	for (let i = 0; i < app_menu_buttons.length; i++)
	{
		app_menu_buttons[i].onclick = function()
		{
			let type = this.className.substring(0, this.className.indexOf(" "));
			type = type.substring(0, type.indexOf("_"));
			load_app(type);
		}
	}
	let csav_button = document.getElementsByClassName("csav_logo")[0];
	csav_button.onclick = function()
	{
		let tmp_bg = document.createElement("div");
		tmp_bg.style.backgroundColor = "rgba(0,0,0,.55)";
		tmp_bg.style.position = "absolute";
		tmp_bg.style.top = "0";
		tmp_bg.style.left = "0";
		tmp_bg.style.width = "100%";
		tmp_bg.style.height = "100%";
		tmp_bg.style.zIndex = "1000";

		tmp_bg.onclick = function()
		{
			this.parentElement.removeChild(document.getElementById("help_cont"));
			this.parentElement.removeChild(this);

		};

		let help_cont = document.createElement("div");
		help_cont.id = "help_cont";
		help_cont.style.backgroundColor = "white";
		help_cont.style.webkitBoxShadow = "0px 5px 5px 0px rgba(10, 10, 20, 0.75)";
		help_cont.style.borderRadius = "7px";
		help_cont.style.borderColor = "rgba(0,0,0,.1)";
		help_cont.style.borderWidth = "1px";
		help_cont.style.width = "30%";
		help_cont.style.height = "31%";
		help_cont.style.margin = "0 auto";
		help_cont.style.top = "36%";
		help_cont.style.position = "relative";
		help_cont.style.zIndex = "1001";
		help_cont.style.textAlign = "center";
		
		let enter_number_span = document.createElement("span");
		enter_number_span.textContent = "Please enter your Name, Phone #, and select an option that best describes your issue.";
		enter_number_span.style.fontSize = "15px";

		let enter_name = document.createElement("input");
		enter_name.style.margin = "10px auto";
		enter_name.style.width = "90%";
		enter_name.style.height = "10%";
		enter_name.type = "text";
		enter_name.style.textAlign = "center";
		enter_name.style.borderRadius = "5px";
		enter_name.style.borderStyle = "solid";
		enter_name.style.borderColor = "rgba(0,0,0,.3)";
		enter_name.style.borderWidth = "1px";
		enter_name.placeholder = "Your name...";

		let enter_number = document.createElement("input");
		enter_number.style.margin = "10px auto";
		enter_number.style.width = "90%";
		enter_number.style.height = "10%";
		enter_number.type = "tel";
		enter_number.style.textAlign = "center";
		enter_number.style.borderRadius = "5px";
		enter_number.style.borderStyle = "solid";
		enter_number.style.borderColor = "rgba(0,0,0,.3)";
		enter_number.style.borderWidth = "1px";
		enter_number.placeholder = "Your phone #...";

		let request_help = document.createElement("div");
		request_help.style.margin = "10px auto";
		request_help.style.backgroundColor = "#252E33";
		request_help.style.width = "25%";
		request_help.style.height = "17%";
		request_help.style.borderRadius = "5px";
		request_help.style.color = "white";
		request_help.style.fontSize = "20px";
		request_help.style.webkitBoxShadow = "0px 2px 3px 0px rgba(10, 10, 20, 0.35)";

		request_help.textContent = "Help";
		
		let prob_sel = document.createElement("select");
		prob_sel.style.margin = "10px auto";
		prob_sel.style.width = "90%";
		prob_sel.style.height = "10%";
		prob_sel.style.textAlign = "center";		

		let prob_opt_0 = document.createElement("option");
		prob_opt_0.value = "NO_SIGNAL";
		prob_opt_0.textContent = "No Signal";
		prob_sel.appendChild(prob_opt_0);

		let prob_opt_1 = document.createElement("option");
		prob_opt_1.value = "NO_AV";
		prob_opt_1.textContent = "No Audio/Video";
		prob_sel.appendChild(prob_opt_1);

		let prob_opt_2 = document.createElement("option");
		prob_opt_2.value = "DIAL_HELP";
		prob_opt_2.textContent = "Dialing Help";
		prob_sel.appendChild(prob_opt_2);

		let prob_opt_3 = document.createElement("option");
		prob_opt_3.value = "APP_HELP";
		prob_opt_3.textContent = "App Help";
		prob_sel.appendChild(prob_opt_3);

		let prob_opt_4 = document.createElement("option");
		prob_opt_4.value = "OTHER";
		prob_opt_4.textContent = "Other";
		prob_sel.appendChild(prob_opt_4);

		request_help.onclick = function()
		{
			let rajax = new XMLHttpRequest();
			rajax.open("POST", "https://www.dialer.withsolutionsrd.com/helpdesk/msg_alert.php");
			rajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
			rajax.send("type=remote_help&data=" + enter_name.value + ":" + enter_number.value + "%20" + prob_sel.options[prob_sel.selectedIndex].value);
			tmp_bg.click();
		};

		help_cont.appendChild(enter_number_span);
		help_cont.appendChild(enter_name);
		help_cont.appendChild(enter_number);
		help_cont.appendChild(prob_sel);
		help_cont.appendChild(request_help);

		document.body.appendChild(tmp_bg);
		document.body.appendChild(help_cont);
	};
	document.getElementsByClassName("mirror_mode")[0].onclick = function()
	{
		if (!settings.mirror_mode_act || settings.mirror_mode_act == undefined)
		{
			settings.mirror_mode_act = true;
			chrome.system.display.setMirrorMode({mode:"normal"});
		}
		else
		{
			settings.mirror_mode_act = false;
			chrome.system.display.setMirrorMode({mode:"off"}, function()
			{
			 // setInterval(function()
			 // {
	          	chrome.system.display.getInfo(function(displayInfo)
	        	{
	        		{
	        			let mon_0 = {};
	        			mon_0.width = displayInfo[0].bounds.width;
	        			mon_0.height = displayInfo[0].bounds.height;
	        			mon_0.top = displayInfo[0].bounds.top;
	        			mon_0.left = displayInfo[0].bounds.left;
	        
	        			let mon_1 = {};
	        			mon_1.width = displayInfo[1].bounds.width;
	        			mon_1.height = displayInfo[1].bounds.height;
	        			mon_1.top = displayInfo[1].bounds.top;
	        			mon_1.left = displayInfo[1].bounds.left;
	        
	        			// establish primary monitor
	        			if (mon_0.width < mon_1.width)
	        			{
	        				let main_window = chrome.app.window.get("main");
	        				let ext_window = chrome.app.window.get("ext");
	        				main_window.innerBounds = {minWidth: mon_0.width, minHeight: mon_0.height, maxWidth: mon_0.width, maxHeight: mon_0.height};
	        				main_window.innerBounds.setPosition(mon_0.left, mon_0.top);
	        				ext_window.innerBounds = {minWidth: mon_1.width, minHeight: mon_1.height, maxWidth: mon_1.width, maxHeight: mon_1.height, left: mon_1.left, top: mon_1.top};
	        			}
	        			else
	        			{
	        			  let main_window = chrome.app.window.get("main");
	        				let ext_window = chrome.app.window.get("ext");
	        				main_window.innerBounds = {minWidth: mon_1.width, minHeight: mon_1.height, maxWidth: mon_1.width, maxHeight: mon_1.height};
	        				main_window.outerBounds.setPosition(mon_1.left, mon_1.top);
	        				ext_window.innerBounds = {minWidth: mon_0.width, minHeight: mon_0.height, maxWidth: mon_0.width, maxHeight: mon_0.height};
	        				ext_window.outerBounds.setPosition(mon_0.left, mon_0.top);
	        			}
	        		}
	        	});
			 // }, 1000);
			});
		}
	}
};