var mtint;
apps =
{
	dialer : "<div class='hoz_app_button_container'>" +
				"<span class='text'>Enter your meeting # or id, select your type of meeting,<br> and press call below to dial in.</span>" +
				"<div class='hoz_app_button overbox larger_text text'>Call</div>" +
				"<div class='hoz_app_button overbox larger_text text'>Leave</div>" + 
				"<input class='app_input text larger_text' type='text' placeholder='Meeting # or ID'><br>" +
				"<br><br><span style='margin-left: 16px;' class='text'>BlueJeans</span><div class='app_toggle_input_bg overbox'><div class='toggle overbox'></div></div><span class='text'>Google Meet<br><br></span><span class='text'>Meeting details will be<br> shown below.</span><br>" +
				"<webview style='float:right;width:700px;height:500px;position:relative;top:-300px;' src='https://dialer.withsolutionsrd.com/schgen/?tok=K221QzDVALCSX0SLYhmEABH9Pm'></webview>" + 
			 "</div><div class='mute audio_m'></div><div class='mute video_m'></div>" +
			 "<div class='left_hoz hoz_app_button_container'>" +
			 	"<span class='text'>Activate auto-dial<br> to have your<br> meetings dialed<br> automatically. <br><br>Your meetings<br> for today will<br> be shown here.<br><br><br><br><br><br><br>You must be signed<br> in to use this<br> feature.</span>" +
			 "</div>",
	control : "<div class='cci_start'>" +
                "<div class='csav_logo cci_logo'></div>" +
                "<div class='text cci_logo_text'>CSAVCCI<br><br>Tap the screen to power on the system.</div>" +
                "<div class='cci_loader'></div>" +
              "</div>",
	// account : "<div class='left_hoz hoz_app_button_container'>" +
	// 		  	"<div class='hoz_app_button overbox larger_text text'>Sign In</div>" +
	// 		  	"<div class='hoz_app_button overbox larger_text text'>Sign Out</div>" +
	// 		  	"<webview class='app_webview'></webview>" +
	// 		  "</div>",
	// settings : "<div style='text-align: center;' class='left_hoz hoz_app_button_container'>" +
	// 		   		"<span class='text'>Mirror Mode</span>" +
	// 		   "</div>",
	chrome : "<div class='left_hoz hoz_app_button_container'>" +
				"<webview class='full_webview' src='https://google.com'></webview>" +
			 "</div>",
	gmail :  "<div class='left_hoz hoz_app_button_container'>" +
				"<webview class='full_webview' src='https://mail.google.com'></webview>" +
			 "</div>",
	drive : "<div class='left_hoz hoz_app_button_container'>" +
				"<webview class='full_webview' src='https://drive.google.com'></webview>" +
			 "</div>",
	jamboard : "<div class='left_hoz hoz_app_button_container'>" +
				"<webview class='full_webview' src='https://jamboard.google.com'></webview>" +
			   "</div>"

}