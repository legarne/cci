window.onload = function()
{
	document.getElementById("handler").addEventListener("click", function()
	{
		for (let i = 0; i < 10; i++)
		{
			console.log(i);
			setTimeout(function()
			{
				var h_block = document.getElementById("h_block_" + i);
				if (i == 9)
				{
					anim(h_block, {marginLeft: window.innerWidth}, 1).anim(function(){document.getElementsByTagName("BODY")[0].removeChild(document.getElementById("splash"))});
				}
				else
				{
					anim(h_block, {marginLeft: window.innerWidth}, 1);
				}
			}, i * 100);
		}
	});
	start_bg_anim();
	chrome.system.display.getInfo(function(displayInfo)
	{
		let mode = "dk";

		if (mode == "dk")
		{
			for (let i = 0; i < 10; i++)
			{
				var h_block = document.createElement("div");
				h_block.id = "h_block_" + i;
				h_block.style.backgroundColor = "#27262E";
				h_block.style.width = displayInfo[0].bounds.width + "px";
				h_block.style.height = (displayInfo[0].bounds.height / 10) + "px";
				document.getElementById("splash").appendChild(h_block);
			}
		}
		else if (mode == "cb")
		{
			for (let i = 0; i < 10; i++)
			{
				var h_block = document.createElement("div");
				h_block.id = "h_block_" + i;
				h_block.style.backgroundColor = "#27262E";
				h_block.style.width = displayInfo[1].bounds.width + "px";
				h_block.style.height = (displayInfo[1].bounds.height / 10) + "px";
				document.getElementById("splash").appendChild(h_block);
			}
		}
	});
};