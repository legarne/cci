var meeting_list = [];
var countdown = 25;
var meeting_started = false;
var countdown_started = false;
var countdownIntv;
var found_join = false;

setInterval(function()
{
	app_controller.set_time();
}, 1000);

function pop_list(force)
{
	if (force)
	{
		meeting_list.shift();
		document.getElementById("schedule_area").textContent = "";
		document.getElementById("schedule_area").style.display = "none";
		var ext_window = chrome.app.window.get("ext").contentWindow;
		ext_window.document.getElementById("meet_info").textContent = "";
		var n_ajax = new XMLHttpRequest();
		n_ajax.onreadystatechange = function()
		{
		   if (this.readyState == 4 && this.status == 200)
		   {  
		       return;
		   }
		}
		n_ajax.open("POST", "https://www.dialer.withsolutionsrd.com/admin/assist_dial.php", true);
		n_ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		n_ajax.send("arr=" + JSON.stringify(meeting_list));
		return;
	}
	let ml_length = meeting_list.length;
	let c_date = Date.now();
	let m_date = new Date(meeting_list[0].date + " " + meeting_list[0].end);
	m_date = m_date.getTime();
	if (ml_length == 0 || c_date < m_date)
	{
		var n_ajax = new XMLHttpRequest();
		n_ajax.onreadystatechange = function()
		{
		   if (this.readyState == 4 && this.status == 200)
		   {  
		       return;
		   }
		}
		n_ajax.open("POST", "https://www.dialer.withsolutionsrd.com/admin/assist_dial.php", true);
		n_ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		n_ajax.send("arr=" + JSON.stringify(meeting_list));
		return;
	}

	if (c_date >= m_date)
	{
		meeting_list.shift();
		document.getElementById("schedule_area").textContent = "";
		document.getElementById("schedule_area").style.display = "none";
		var ext_window = chrome.app.window.get("ext").contentWindow;
		ext_window.document.getElementById("meet_info").textContent = "";
		pop_list();
	}
}

function leave_meeting_display(set)
{
	var leave_meeting_button = document.getElementById("leave_meeting_button");
	if (set)
		anim(leave_meeting_button, {top :"0"}, 1);
	else
		anim(leave_meeting_button, {top :"-200px"}, 1);
}

function first_update()
{
	var i_ajax = new XMLHttpRequest();
    i_ajax.onreadystatechange = function()
    {
        if (this.readyState == 4 && this.status == 200)
        {  
            var up_meeting_list = JSON.parse(decodeURIComponent(this.responseText));
            meeting_list = up_meeting_list;
            pop_list();
        }
    }
    i_ajax.open("POST", "https://www.dialer.withsolutionsrd.com/admin/update.php", true);
    i_ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    i_ajax.send("");
}

function start_meeting(dial_id)
{
	leave_meeting_display(true);
	var ext_window = chrome.app.window.get("ext").contentWindow;
	if (ext_window.document.getElementById("meet_view") != undefined)
		ext_window.document.getElementsByTagName("BODY")[0].removeChild(ext_window.document.getElementById("meet_view"));
	var webview = document.createElement("webview");
	webview.style.width = "100%";
	webview.style.height = "100%";
	webview.id = "meet_view";
	webview.src = "https://bluejeans.com/" + dial_id + "/?embed=true&fullscreen=true&name=Tahoe%20Conference%20Room";
	ext_window.document.getElementsByTagName("BODY")[0].appendChild(webview);
	webview.addEventListener("permissionrequest", function(e)
	{
		e.request.allow();
	});
	meeting_started = true;
	countdown_started = false;
}

const app_controller = 
{
	layout : {},
	dialer_open : false,
	night_mode : false,
	set_layout : function(layout, mode, callback)
	{
		let content = document.getElementById("content");
		while (content.firstChild)
			content.removeChild(content.firstChild);

		chrome.system.display.getInfo(function(display_info)
		{
			if (mode == "cb")
			{			
				if (layout == "full")
				{
					let container = document.createElement("div");
					container.id = "container";
					container.style.width = display_info[1].bounds.width + "px";
					container.style.height = display_info[1].bounds.height + "px";
					// container.style.backgroundColor = "red";
					content.appendChild(container);
					app_controller.layout.container = container;
					// this.layout.container = layout;
				}
				else if (layout == "half")
				{
					let l_container = document.createElement("div");
					l_container.id = "l_container";
					l_container.style.width = (display_info[1].bounds.width / 2) + "px";
					l_container.style.height = display_info[1].bounds.height + "px";
					l_container.style.backgroundColor = "red";
					l_container.style.float = "left";
					l_container.style.margin = "0";
					content.appendChild(l_container);
					
					let r_container = document.createElement("div");
					r_container.id = "r_container";
					r_container.style.width = (display_info[1].bounds.width / 2) + "px";
					r_container.style.height = display_info[1].bounds.height + "px";
					r_container.style.backgroundColor = "blue";
					r_container.style.float = "right";
					r_container.style.margin = "0";
					content.appendChild(r_container);
					
					let mult_container = [];
					mult_container.push(l_container);
					mult_container.push(r_container);
					app_controller.layout.container = mult_container;
				}
			}
			else if (mode == "dk")
			{
				if (layout == "full")
				{
					let container = document.createElement("div");
					container.id = "container";
					container.style.width = display_info[0].bounds.width + "px";
					container.style.height = display_info[0].bounds.height + "px";
					// container.style.backgroundColor = "red";
					content.appendChild(container);
					app_controller.layout.container = container;
					// this.layout.container = layout;
				}
				else if (layout == "half")
				{
					let l_container = document.createElement("div");
					l_container.id = "l_container";
					l_container.style.width = (display_info[0].bounds.width / 2) + "px";
					l_container.style.height = display_info[0].bounds.height + "px";
					l_container.style.backgroundColor = "red";
					l_container.style.float = "left";
					l_container.style.margin = "0";
					content.appendChild(l_container);
					
					let r_container = document.createElement("div");
					r_container.id = "r_container";
					r_container.style.width = (display_info[0].bounds.width / 2) + "px";
					r_container.style.height = display_info[0].bounds.height + "px";
					r_container.style.backgroundColor = "blue";
					r_container.style.float = "right";
					r_container.style.margin = "0";
					content.appendChild(r_container);
					
					let mult_container = [];
					mult_container.push(l_container);
					mult_container.push(r_container);
					app_controller.layout.container = mult_container;
				}
			}

			callback(app_controller.layout);
		});
	},
	get_layout : function()
	{
		return this.layout;
	},
	load_app : function(type)
	{
		if (type == "base")
		{
			let layout = this.set_layout("full", "cb",
			function(response)
			{
				setInterval(function()
				{
					first_update();
					if (meeting_started)
					{
						var ext_window = chrome.app.window.get("ext").contentWindow;
						var webview = ext_window.document.getElementById("meet_view");
						webview.executeScript(
						{
							code: "if (window.getComputedStyle(document.getElementsByClassName('primaryOption joinComputer')[0].parentElement).width != 'auto')" +
									"document.getElementsByClassName('primaryOption joinComputer')[0].click();" +
									"setTimeout(function() {if (document.getElementsByClassName('enter')[0].style.display != 'none') {" +
									"document.getElementsByClassName('enter')[0].click();}}, 5000);"
						});
						webview.executeScript(
						{
							code: "if (document.getElementsByClassName('slideOutPanel fullheight icon_labels')[0] != undefined)" + 
									"{ document.getElementsByClassName('slideOutPanel fullheight icon_labels')[0].style.display = 'none';}"
						});
						webview.executeScript(
						{
							code: "if (document.getElementsByClassName('toolbar fullheight icon_labels')[0] != undefined)" + 
									"{ document.getElementsByClassName('toolbar fullheight icon_labels')[0].style.display = 'none';}"
						});
					}
					if (meeting_list[0] != undefined && !meeting_started)
					{
						let c_date = Date.now();
						let m_date = new Date(meeting_list[0].date + " " + meeting_list[0].start);
						m_date = m_date.getTime();
						document.getElementById("schedule_area").textContent = meeting_list[0].name + " " + meeting_list[0].start + "-" + meeting_list[0].end;
						document.getElementById("schedule_area").style.display = "block";
						if (c_date > m_date)
						{
							var ext_window = chrome.app.window.get("ext").contentWindow;
							countdown_started = true;
							meeting_started = false;
							countdown -= 1;
							ext_window.document.getElementById("meet_info").textContent = "Your meeting [" + meeting_list[0].name + "] will begin in " + countdown + "...";

							if (countdown <= 0)
							{
								start_meeting(meeting_list[0].id);
							}
						}
					}
				}, 1000);
				var clock_area = document.createElement("div");
				clock_area.style.margin = "0 auto";
				clock_area.style.fontSize = "40em";
				clock_area.style.width = response.container.style.width;
				clock_area.style.textAlign = "center";
				var clock = document.createElement("span");
				clock.id = "clock";
				clock.style.color = "#B23466";
				clock.style.textShadow = "0px 3px 5px rgba(75, 75, 75, .95)";
				var a_p_t = document.createElement("span");
				a_p_t.id = "a_p";
				a_p_t.style.color = "#B23466";
				a_p_t.style.textShadow = "0px 2px 3px rgba(75, 75, 75, .65)";
				a_p_t.style.fontSize = "50px";
				
				var leave_meeting_button = document.createElement("div");
				leave_meeting_button.id = "leave_meeting_button";
				leave_meeting_button.style.margin = "0 auto";
				leave_meeting_button.style.width = "300px";
				leave_meeting_button.style.height = "100px";
				leave_meeting_button.style.backgroundColor = "#C70A0A";
				leave_meeting_button.style.position = "absolute";
				leave_meeting_button.style.top = "-200px";
				leave_meeting_button.style.borderRadius = "0 0 10px 10px";
				leave_meeting_button.textContent = "Leave";
				leave_meeting_button.style.fontSize = "50px";
				leave_meeting_button.style.textAlign = "center";
				leave_meeting_button.style.webkitBoxShadow = "0px 5px 5px 0px rgba(50, 50, 50, 0.75)";
				leave_meeting_button.style.borderBottom = "solid";
				leave_meeting_button.style.borderWidth = "1px";
				leave_meeting_button.style.borderColor = "white";

				leave_meeting_button.style.color = "#420202";
				leave_meeting_button.style.left = ((parseInt(response.container.style.width) / 2) - (parseInt(leave_meeting_button.style.width) / 2)) + "px";
				response.container.appendChild(leave_meeting_button);

				leave_meeting_button.onclick = function(e)
				{
					e = window.event || e;
					if (this === e.target)
					{
						leave_meeting_display(false);
						meeting_started = false;
						countdown_started = false;
						countdown = 25;

						var ext_window = chrome.app.window.get("ext").contentWindow;
						ext_window.document.getElementsByTagName("BODY")[0].removeChild(ext_window.document.getElementById("meet_view"));
						pop_list(true);
					}
				};

				var audio_mute_button = document.createElement("div");
				audio_mute_button.id = "audio_mute_button";
				audio_mute_button.style.width = "100px";
				audio_mute_button.style.height = "100px";
				audio_mute_button.style.backgroundColor = "#1FCCF2";
				audio_mute_button.style.position = "absolute";
				audio_mute_button.style.top = "10px";
				audio_mute_button.style.left = "-300px";
				audio_mute_button.style.borderRadius = "100px";
				audio_mute_button.style.borderStyle = "solid";
				audio_mute_button.style.borderColor = "white";
				audio_mute_button.style.borderWidth = "1px";
				audio_mute_button.style.backgroundImage = "url('icons8-microphone-96.png')";
				audio_mute_button.style.backgroundSize = "100% 100%";
				audio_mute_button.style.backgroundRepeat = "no-repeat";
				audio_mute_button.style.webkitBoxShadow = "0px 5px 5px 0px rgba(50, 50, 50, 0.75)";
				audio_mute_button.style.zIndex = "99";
				audio_mute_button.is_clicked = false;

				audio_mute_button.onclick = function()
				{
					if (!this.is_clicked)
					{
						this.style.backgroundColor = "grey";
						this.is_clicked = true;
					}
					else
					{
						this.style.backgroundColor = "#1FCCF2";
						this.is_clicked = false;
					}

					var ext_window = chrome.app.window.get("ext").contentWindow;
					var webview = ext_window.document.getElementById("meet_view");
					webview.executeScript
					({code: 
						"var audio_mute = document.getElementsByClassName('mute');" +
						"audio_mute[1].click();"
					});
				};

				var video_mute_button = document.createElement("div");
				video_mute_button.id = "video_mute_button";
				video_mute_button.style.width = "100px";
				video_mute_button.style.height = "100px";
				video_mute_button.style.backgroundColor = "#1FCCF2";
				video_mute_button.style.position = "absolute";
				video_mute_button.style.top = "10px";
				video_mute_button.style.left = "500px";
				video_mute_button.style.borderRadius = "100px";
				video_mute_button.style.borderStyle = "solid";
				video_mute_button.style.borderColor = "white";
				video_mute_button.style.borderWidth = "1px";
				video_mute_button.style.backgroundImage = "url('icons8-video-call-filled-500.png')";
				video_mute_button.style.backgroundSize = "75% 75%";
				video_mute_button.style.backgroundRepeat = "no-repeat";
				video_mute_button.style.backgroundPosition = "15px 13px";
				video_mute_button.style.webkitBoxShadow = "0px 5px 5px 0px rgba(50, 50, 50, 0.75)";
				video_mute_button.style.zIndex = "99";
				video_mute_button.is_clicked = false;
				video_mute_button.onclick = function()
				{
					if (!this.is_clicked)
					{
						this.style.backgroundColor = "grey";
						this.is_clicked = true;
					}
					else
					{
						this.style.backgroundColor = "#1FCCF2";
						this.is_clicked = false;
					}
					var ext_window = chrome.app.window.get("ext").contentWindow;
					var webview = ext_window.document.getElementById("meet_view");
					webview.executeScript
					({code: 
						"var audio_mute = document.getElementsByClassName('mute');" +
						"audio_mute[0].click();"
					});
				};


				leave_meeting_button.appendChild(audio_mute_button);
				leave_meeting_button.appendChild(video_mute_button);

				clock_area.appendChild(clock);
				clock_area.appendChild(a_p_t);
				response.container.appendChild(clock_area);
				app_controller.set_time();

				var num_area = document.createElement("div");
				num_area.style.margin = "0 auto";
				num_area.style.fontSize = "60px";
				num_area.style.width = response.container.style.width;
				num_area.style.textAlign = "center";
				num_area.style.marginTop = "-60px";
				var num = document.createElement("span");
				num.id = "num";
				num.style.color = "#B23466";
				num.style.textShadow = "0px 2px 3px rgba(75, 75, 75, .85)";
				num_area.appendChild(num);
				response.container.appendChild(num_area);

				var schedule_area = document.createElement("div");
				schedule_area.style.backgroundColor = "#B23466";
				schedule_area.id = "schedule_area";
				schedule_area.style.width = "500px";
				schedule_area.style.height = "100px";
				schedule_area.style.margin = "0 auto";
				schedule_area.style.borderRadius = "10px"
				schedule_area.style.display = "none";
				schedule_area.style.fontSize = "30px";
				schedule_area.style.textAlign = "center";
				schedule_area.style.webkitBoxShadow = "0px 5px 5px 0px rgba(50, 50, 50, 0.75)";
				response.container.appendChild(schedule_area);
				schedule_area.onclick = function()
				{
					start_meeting(meeting_list[0].id);
				};

				var phone_button = document.createElement("div");
				phone_button.id = "phone_button";
				phone_button.style.width = "200px";
				phone_button.style.height = "150px";
				phone_button.style.backgroundColor = "#B23466";
				phone_button.style.position = "absolute";
				phone_button.style.borderRadius = "100px 0 0 0";
				phone_button.style.backgroundImage = "url('icons8-phone-96.png')";
				phone_button.style.backgroundSize = "55% 75%";
				phone_button.style.backgroundPosition = "60px 25px";
				phone_button.style.backgroundRepeat = "no-repeat";
				phone_button.style.webkitBoxShadow = "-2px -1px 12px 0px rgba(50, 50, 50, 0.6)";
				phone_button.style.borderTop = "solid";
				phone_button.style.borderColor = "white";
				phone_button.style.borderWidth = "2px";
				phone_button.style.left = (parseInt(response.container.style.width) - 200) + "px";
				phone_button.style.top = (parseInt(response.container.style.height) - 150) + "px";
				phone_button.onclick = function()
				{
					let dialer_area = document.getElementById("dialer_area");
					if (!app_controller.dialer_open)
					{
						anim(this, {left: (parseInt(this.style.left) - 500)}, .5);
						anim(dialer_area, {left: (parseInt(dialer_area.style.left) - 500)}, .4);
						app_controller.dialer_open = true;
					}
					else
					{
						anim(this, {left: (parseInt(response.container.style.width) - 200)}, .45);
						anim(dialer_area, {left: response.container.style.width}, .5);
						app_controller.dialer_open = false;
					}
				};

				response.container.appendChild(phone_button);

				var dialer_area = document.createElement("div");
				dialer_area.id = "dialer_area";
				dialer_area.style.width = "500px";
				dialer_area.style.height = "600px";
				dialer_area.style.backgroundColor = "#B23466";
				dialer_area.style.position = "absolute";
				dialer_area.style.top = (parseInt(response.container.style.height) - 600) + "px";
				dialer_area.style.left = response.container.style.width;
				dialer_area.style.borderRadius = "100px 0 0 0";
				dialer_area.style.webkitBoxShadow = "-2px -1px 12px 0px rgba(50, 50, 50, 0.6)";
				dialer_area.style.borderTop = "solid";
				dialer_area.style.borderColor = "white";
				dialer_area.style.borderWidth = "2px";

				var dial_table = document.createElement("table");
				dial_table.style.width = "380px";
				dial_table.style.height = "500px";
				dial_table.style.position = "absolute";
				dial_table.style.left = "70px";
				var dial_rows = [];
				var button_arr = [1,2,3,4,5,6,7,8,9,"*",0,"#"];
				var button_count = 0;

				for (let i = 0; i < 4; i++)
				{
					let row = document.createElement("tr");
					dial_rows.push(row);
					for (let n = 0; n < 3; n++)
					{
						let data = document.createElement("td");
						dial_rows[i].appendChild(data);

						let button = document.createElement("div");
						button.style.width = "100px";
						button.style.height = "100px";
						button.style.borderRadius = "10px";
						button.style.fontSize = "55px";
						button.style.textAlign = "center";
						button.style.lineHeight = "1.8";
						button.style.backgroundColor = "#E33659";
						button.style.webkitBoxShadow = "0px 2px 8px 0px rgba(50, 50, 50, 0.8)";
						button.style.borderTop = "solid";
						button.style.borderColor = "white";
						button.style.borderWidth = "1px";
						button.textContent = button_arr[button_count];
						button.style.color = "#2E1218";
						button.onclick = function()
						{
							if (this.textContent == "*")
								document.getElementById("num").textContent = document.getElementById("num").textContent.substr(0, document.getElementById("num").textContent.length - 1);
							else
								document.getElementById("num").textContent = document.getElementById("num").textContent + this.textContent;

							this.style.backgroundColor = "#3D1A28";
							this.style.color = "white";
							this.style.webkitBoxShadow = "inset 0px 0px 24px 5px rgba(50, 50, 50, 0.47)";
							this.style.borderTop = "none";
							this.style.borderBottom = "solid";
							setTimeout(function(el)
							{
								el.style.backgroundColor = "#E33659";
								el.style.color = "#2E1218";
								el.style.webkitBoxShadow = "0px 2px 8px 0px rgba(50, 50, 50, 0.8)";
								el.style.borderTop = "solid";
								el.style.borderColor = "white";
								el.style.borderWidth = "1px";
								el.style.borderBottom = "none";
							}, 100, this);
						};
						button_count++;
						data.appendChild(button);
					}
					dial_table.appendChild(dial_rows[i]);
				}
				dialer_area.appendChild(dial_table);
				response.container.appendChild(dialer_area);

				var call_button = document.createElement("div");
				call_button.id = "call_button";
				call_button.style.width = parseInt(dial_table.style.width) - 24 + "px";
				call_button.style.height = "80px";
				call_button.style.backgroundColor = "#E33659";
				call_button.style.webkitBoxShadow = "0px 2px 8px 0px rgba(50, 50, 50, 0.8)";
				call_button.style.borderTop = "solid";
				call_button.style.borderColor = "white";
				call_button.style.borderWidth = "1px";
				call_button.style.position = "absolute";
				call_button.style.left = "70px";
				call_button.style.top = "500px";
				call_button.style.borderRadius = "10px";
				call_button.style.fontSize = "55px";
				call_button.style.textAlign = "center";
				call_button.style.lineHeight = "1.4";
				call_button.textContent = "Call";
				call_button.style.color = "#2E1218";
				dialer_area.appendChild(call_button);
				call_button.onclick = function()
				{
					document.getElementById("phone_button").click();
					var meet_id = document.getElementById("num").textContent;

					start_meeting(meet_id);
					document.getElementById("num").textContent = "";

					this.style.backgroundColor = "#3D1A28";
					this.style.color = "white";
					this.style.webkitBoxShadow = "inset 0px 0px 24px 5px rgba(50, 50, 50, 0.47)";
					this.style.borderTop = "none";
					this.style.borderBottom = "solid";
					setTimeout(function(el)
					{
						el.style.backgroundColor = "#E33659";
						el.style.color = "#2E1218";
						el.style.webkitBoxShadow = "0px 2px 8px 0px rgba(50, 50, 50, 0.8)";
						el.style.borderTop = "solid";
						el.style.borderColor = "white";
						el.style.borderWidth = "1px";
						el.style.borderBottom = "none";
					}, 100, this);
				};
				var light_button = document.createElement("div");
				light_button.id = "light_button";
				light_button.style.width = "200px";
				light_button.style.height = "150px";
				light_button.style.backgroundColor = "#B23466";
				light_button.style.position = "absolute";
				light_button.style.borderRadius = "0 100px 0 0";
				light_button.style.backgroundImage = "url('light_icon.png')";
				light_button.style.backgroundSize = "55% 75%";
				light_button.style.backgroundPosition = "50px 25px";
				light_button.style.backgroundRepeat = "no-repeat";
				light_button.style.webkitBoxShadow = "-2px -1px 12px 0px rgba(50, 50, 50, 0.6)";
				light_button.style.borderTop = "solid";
				light_button.style.borderColor = "white";
				light_button.style.borderWidth = "2px";
				light_button.style.left = "-30px";
				light_button.style.top = (parseInt(response.container.style.height) - 150) + "px";
				light_button.onclick = function()
				{
					var ext_window = chrome.app.window.get("ext").contentWindow;
					if (!app_controller.night_mode)
					{
						document.getElementsByTagName("BODY")[0].style.backgroundColor = "#27262E";
						ext_window.document.getElementsByTagName("BODY")[0].style.backgroundColor = "#27262E";
						document.getElementById("clock").style.color = "#2298B2";
						document.getElementById("a_p").style.color = "#2298B2";
						document.getElementById("num").style.color = "#2298B2";
						document.getElementById("phone_button").style.backgroundColor = "#2298B2";
						document.getElementById("light_button").style.backgroundColor = "#2298B2";
						document.getElementById("dialer_area").style.backgroundColor = "#2298B2";
						app_controller.night_mode = true;
					}
					else
					{
						document.getElementsByTagName("BODY")[0].style.backgroundColor = "white";
						ext_window.document.getElementsByTagName("BODY")[0].style.backgroundColor = "white";
						document.getElementById("clock").style.color = "#B23466";
						document.getElementById("a_p").style.color = "#B23466";
						document.getElementById("num").style.color = "#B23466";
						document.getElementById("phone_button").style.backgroundColor = "#B23466";
						document.getElementById("light_button").style.backgroundColor = "#B23466";
						document.getElementById("dialer_area").style.backgroundColor = "#B23466";
						app_controller.night_mode = false;
					}
				};

				response.container.appendChild(light_button);

			});
		}
	},
	set_time : function()
	{
		let d = new Date();
		let local_time = d.toLocaleTimeString();
		let formatted_time = local_time.substring(0, local_time.lastIndexOf(":"));
		let a_p = local_time.substring(local_time.lastIndexOf(" "), local_time.length);
		
		let clock = document.getElementById("clock");
		let a_p_t = document.getElementById("a_p");
		if (clock != undefined && a_p != undefined)
		{
			clock.textContent = formatted_time;
			a_p_t.textContent = a_p;
		}
	}
}

// Call Button
// Show Upcoming
// night mode
// Auto Schedule, One Meeting
// One-click to dial