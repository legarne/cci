function start_bg_anim()
{
	var header = document.getElementById("header");
	var size = innerHeight;
	for (let i = 0; i < 50; i++)
	{
		let b_size = size * (Math.random() * (1.0 - 0.1) + 0.1);
		let start = null;
		let px_counter = 0;

		let div = document.createElement("div");
		div.className = "blocks";
		div.style.height = b_size + "px";
		div.style.backgroundColor = "hsla(" + (Math.random() * (360 - 0) + 0) + ", 100%, 90%, 0.07)";
		let init_pos = (Math.random() * (innerWidth - 0) + 0);
		div.style.left = init_pos + "px";

		header.appendChild(div);
		
		let rand_count = null;
		let first = false;
		function run_anim(time)
		{	
			if (start == null) start = time;
			if (rand_count == null) rand_count = Math.random() * (1.0 - 0.1) + 0.1;
			let progress = time - start;
			if (parseInt(div.style.left) >= innerWidth + 10)
			{
				div.style.left = "-200px";
				progress = 0;
				px_counter = -200;
				start = null;
				rand_count = null;
				first = true;
			}
			if (!first)
				div.style.left = ((px_counter + init_pos) + ((progress / 25) * rand_count)) + "px";
			else
				div.style.left = (px_counter + ((progress / 25) * rand_count)) + "px";

			requestAnimationFrame(run_anim);
		}
		requestAnimationFrame(run_anim);
	}
};