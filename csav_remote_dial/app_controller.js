function check_pin()
{
	let pin = document.getElementById("entered_pass");
	let auth_status = document.getElementById("auth_status");
	if (pin.textContent.length == 4)
	{
		if (pin.textContent == "7989")
		{
			let controller = Object.create(app_controller);
			controller.load_app("base");
			pin.textContent = "";
			auth_status.textContent = "Launching app...";
			let csavlogo = document.getElementById("csavlogo");
			anim(document.getElementById("pin_container"), {opacity: 0}, 1);
			anim(document.getElementById("auth_status"), {opacity: 0}, 1);
			anim(csavlogo, {top: (window.innerHeight / 2) - 180}, 1).anim(function()
				{
					anim(csavlogo, {opacity: 0}, 2.5).anim(function()
						{
							document.getElementsByTagName("BODY")[0].removeChild(document.getElementById("csavlogo"));
							document.getElementsByTagName("BODY")[0].removeChild(document.getElementById("splash"));
							document.getElementsByTagName("BODY")[0].removeChild(document.getElementById("pin_screen"));
						});
					chrome.app.window.get("ext").contentWindow.document.getElementById("handler").click();
					for (let i = 0; i < 10; i++)
					{
						setTimeout(function()
						{
							var h_block = document.getElementById("h_block_" + i);
							anim(h_block, {marginLeft: window.innerWidth}, 1);
						}, i * 100);
					}
				});
		}
		else
		{
			pin.textContent = "";
			auth_status.textContent = "Wrong pin.";
		}
	}
}
window.onload = function()
{
	let csavlogo = document.getElementById("csavlogo");
	document.getElementById("auth_status").textContent = "Enter pin";
	csavlogo.style.left = ((window.innerWidth / 2) - (400 / 2)) + "px";
	for (let i = 0; i < 10; i++)
	{
		var h_block = document.createElement("div");
		h_block.id = "h_block_" + i;
		h_block.style.backgroundColor = "#27262E";
		h_block.style.width = "100%";
		h_block.style.height = "10%";
		document.getElementById("splash").appendChild(h_block);
	}
	start_bg_anim();

	let pin_container = document.getElementById("pin_container");
	pin_container.style.position = "relative";
	pin_container.style.top = "100px";
	pin_container.style.borderRadius = "10px";


	let pin_table = document.createElement("table");
	pin_container.appendChild(pin_table);
	pin_table.style.width = "100%";
	pin_table.style.height = "100%";

	let pin_set = [7,8,9,4,5,6,1,2,3,0];
	let pin_counter = 0;

	var passcode = "";

	let clear_button = document.createElement("div");
	clear_button.textContent = "Clear";
	clear_button.style.position = "absolute";
	clear_button.style.top = "-80px";
	clear_button.style.left = "0";
	clear_button.style.width = "99%";
	clear_button.style.height = "70px";
	clear_button.style.backgroundColor = "grey";
	clear_button.style.fontSize = "35px";
	// clear_button.style.textAlign = "center";
	clear_button.style.lineHeight = "1.5";
	clear_button.style.borderRadius = "10px";
	clear_button.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8)";
	clear_button.style.borderStyle = 'solid';
	clear_button.style.borderWidth = '2px';
	clear_button.style.borderColor = 'white';

	clear_button.onclick = function()
	{
		let pin = document.getElementById("entered_pass").textContent = "";
	};

	pin_container.appendChild(clear_button);

	for (let i = 0; i < 4; i++)
	{
		let pin_row = document.createElement("tr");
		if (i == 3)
		{
			let pin_data = document.createElement("td");
			let pin = document.createElement("div");
			pin.textContent = "0";
			pin.style.width = "313%";
			pin.style.height = "100%";
			pin.style.backgroundColor = "grey";
			pin.style.fontSize = "35px";
			pin.style.textAlign = "center";
			pin.style.lineHeight = "1.5";
			pin.style.borderRadius = "10px";
			pin.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8)";
			pin.onclick = function()
			{
				document.getElementById("entered_pass").textContent = document.getElementById("entered_pass").textContent + this.textContent;
				pin.style.backgroundColor = "#444444";
				pin.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8) inset";
				pin.style.color = "white";
				check_pin();
				setTimeout(function()
				{
					pin.style.backgroundColor = "grey";
					pin.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8)";
					pin.style.color = "black";
				}, 100);
			}
			pin_counter++;
			pin_data.appendChild(pin);
			pin_row.appendChild(pin_data);
		}
		else
		{		
			for (let n = 0; n < 3; n++)
			{
				let pin_data = document.createElement("td");
				let pin = document.createElement("div");
				pin.textContent = pin_set[pin_counter];
				pin.style.width = "100%";
				pin.style.height = "100%";
				pin.style.backgroundColor = "grey";
				pin.style.fontSize = "35px";
				pin.style.textAlign = "center";
				pin.style.lineHeight = "1.5";
				pin.style.borderRadius = "10px";
				pin.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8)";
				pin.onclick = function()
				{
					document.getElementById("entered_pass").textContent = document.getElementById("entered_pass").textContent + this.textContent;
					pin.style.backgroundColor = "#444444";
					pin.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8) inset";
					pin.style.color = "white";
					check_pin();
					setTimeout(function()
					{
						pin.style.backgroundColor = "grey";
						pin.style.boxShadow = "0px 3px 3px rgba(20,20,20, .8)";
						pin.style.color = "black";
					}, 100);
				}
				pin_counter++;
				pin_data.appendChild(pin);
				pin_row.appendChild(pin_data);
			}
		}
		pin_table.appendChild(pin_row);
	}
};