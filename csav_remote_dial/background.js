let settings = {};

function create_window(monitor, index_file, idp)
{
	chrome.app.window.create(index_file,
	{
		id: idp,
		innerBounds: {minWidth: monitor.width, minHeight: monitor.height, maxWidth: monitor.width, maxHeight: monitor.height},
		frame: "none",
		resizable: false,
		alwaysOnTop: false
	}, function(win)
	{
		win.innerBounds.setPosition(monitor.left, monitor.top);
		win.setAlwaysOnTop(false);
		win.fullscreen();
	});
}

chrome.app.runtime.onLaunched.addListener(function()
{
	chrome.system.display.getInfo(function(displayInfo)
	{
		{
			let mon_0 = {};
			mon_0.width = displayInfo[0].bounds.width;
			mon_0.height = displayInfo[0].bounds.height;
			mon_0.top = displayInfo[0].bounds.top;
			mon_0.left = displayInfo[0].bounds.left;

			let mon_1 = {};
			mon_1.width = displayInfo[1].bounds.width;
			mon_1.height = displayInfo[1].bounds.height;
			mon_1.top = displayInfo[1].bounds.top;
			mon_1.left = displayInfo[1].bounds.left;

			// establish primary monitor
			if (mon_0.width < mon_1.width)
			{
				create_window(mon_0, "index.html", "main");
				create_window(mon_1, "index_ext.html", "ext");
			}
			else
			{
				create_window(mon_1, "index.html", "main");
				create_window(mon_0, "index_ext.html", "ext");
			}
		}
	});
});
